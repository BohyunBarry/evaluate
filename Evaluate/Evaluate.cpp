// Evaluate.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <cctype>
#include "string.h"
#include <string>
#include <vector>

using namespace std;


bool evaluate(const char *expression, int &result);
bool expressions(const char **expr, int &op, bool inside = false);
bool eval_number(const char **num, int &res);
char ignore_withespace(const char **space);

int main()
{
	int res;
	const char *expression = "1+3";
	if (evaluate(expression, res)) {
		cout << "Input: \t\t" << "1+3 "<< "\tResult: \t\t" <<res << "\tReturn Code: " << evaluate(expression, res) << endl;
	}
	else {
		cout << "Input: \t\t" << "1+3 " << "\tResult: \t\t" << "N/A" << "\tReturn Code: " << evaluate(expression, res) << endl;
	}
	const char *expression_1 = "(1 + 3) * 2";
	if (evaluate(expression_1, res)) {
		cout << "Input: \t\t" << "(1 + 3) * 2 " << "\tResult: \t\t" << res << "\tReturn Code: " << evaluate(expression_1, res) << endl;
	}
	else {
		cout << "Input: \t\t" << "(1 + 3) * 2 " << "\tResult: \t\t" << "N/A" << "\tReturn Code: " << evaluate(expression_1, res) << endl;
	}
	const char *expression_2 = "(4 / 2) + 6";
	if (evaluate(expression_2, res)) {
		cout << "Input: \t\t" <<"(4 / 2) + 6" << "\tResult: \t\t" << res << "\tReturn Code: " << evaluate(expression_2, res) << endl;
	}
	else {
		cout << "Input: \t\t" << "(4 / 2) + 6" << "\tResult: \t\t" << "N/A" << "\tReturn Code: " << evaluate(expression_2, res) << endl;
	}
	const char *expression_3 = "4 + (12 / (1 * 2))";
	if (evaluate(expression_3, res)) {
		cout << "Input: \t\t" << "4 + (12 / (1 * 2))" << "\tResult: \t\t" << res << "\tReturn Code: " << evaluate(expression_3, res) << endl;
	}
	else {
		cout << "Input: \t\t" << "4 + (12 / (1 * 2))" << "\tResult: \t\t" << "N/A" << "\tReturn Code: " << evaluate(expression_3, res) << endl;
	}
	const char *expression_4 = "(1 + (12 * 2)";
	if (evaluate(expression_4, res)) {
		cout << "Input: \t\t" << "(1 + (12 * 2)" << "\tResult: \t\t" << res << "\tReturn Code: " << evaluate(expression_4, res) << endl;
	}
	else {
		cout << "Input: \t\t" << "(1 + (12 * 2)" << "\tResult: \t\t" << "N/A" << "\tReturn Code: " << evaluate(expression_4, res) << endl;
	}
	return 0;
}
//Ignore the whitespace, using double pointer here, pointer to pointer
char ignore_withespace(const char **space)
{
	while (**space == ' ')
	{
		//skip the space
		++(*space);
	}
	return **space;
}
//evaluate a parenthesized expression or a number
bool eval_number(const char **num, int &res)
{
	char ignore = ignore_withespace(num);
	//if there has a bracket, then do the bracket calculation first
	if (ignore == '(') 
	{
		++(*num);
		if (!expressions(num, res, true))
			return false;
		++(*num);
		return true;
	}
	//check whether ignore is a decimal digit character
	if (isdigit(ignore))
	{
		res = 0;
		while (isdigit(ignore))
		{
			res = 10*res + ignore - '0';
			ignore = *(++(*num));
		}
		return true;
	}
	return false;
}

//evaluate a chain of + - * / operations  
bool expressions(const char **expr, int &result, bool inside)
{
	//If there doesn't have a parenthesized expression or a number
	if (!eval_number(expr, result))
		return false;
	char op;
	while ((op = ignore_withespace(expr)) && (op == '+' || op == '-' || op == '*' || op == '/'))
	{
		++(*expr);
		int actualResult;
		if (!eval_number(expr, actualResult))
			return false;
		//Show the calculate sequence
		vector<int> operands;
		vector<char> operators;
		char tempExpression[100];
		//strcyp_s will not withe more than 100 chars; copy a string
		strcpy_s(tempExpression, sizeof(tempExpression), *expr);
		char *next_token;
		//During the first read, establish the char string and get the first token
		char* c = strtok_s(tempExpression, " ", &next_token);
		while (c != NULL)
		{
			printf("%s\n", c);
			// the first occurrence of a substring{/ * - + =} in a string c
			if (strstr("/*-+=", c) == NULL) {
				operands.push_back(atoi(c));
			}
			else {
				operators.push_back(c[0]);
			}
			c = strtok_s(NULL, " ", &next_token);
		}
		//using switch statement to transfer control to one of the operations
		switch (op)
		{
		case '+':
		//this operator is combination of '+' and '=' operators.This operator first adds the current value of the variable on left to the value on right and then assigns the result to the variable on the left.
			result += actualResult;
			break;
		case '-':
		//this operator is combination of '-' and '=' operators.This operator first subtracts  the current value of the variable on left to the value on right and then assigns the result to the variable on the left.
			result -= actualResult;
			break;
		case '*':
		//this operator is combination of '*' and '=' operators.This operator first multiplies   the current value of the variable on left to the value on right and then assigns the result to the variable on the left.
			result *= actualResult;
			break;
		case '/':
		//this operator is combination of '/' and '=' operators.This operator first divides    the current value of the variable on left to the value on right and then assigns the result to the variable on the left.
			result /= actualResult;
			break;
		default:
			break;
		}
	}
	// Finish with the close brackets
	return inside ? op == ')' : !op;
}
//wrap the API inside
bool evaluate(const char *expression, int &result) 
{
	return expressions(&expression, result);
}

